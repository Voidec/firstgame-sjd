package com.starjdevs.pacrunner.core.audio;

import android.content.Context;

/**
 * @author Andy Tyurin
 */
public interface MusicInit {
    /**
     * Init musics by creating them inside method.
     * It need for filling initial data of musics to audio manager and for their future control.
     * @param a Audio Manager that invoke initialization
     * @param c Context that run our music (activity)
     */
    public void init(AudioManager a, Context c);

    /**
     * Fire when music's composition end. (song completely ends)
     * @param a Audio Manager that invoke initialization
     * @param m Instance of music that starts playing
     * @param loopStage number of playing loop
     */
    public void onMusicComplete(AudioManager a, Music m, int loopStage);

    /**
     * Fire when some problems occur in music play
     * @param a Audio Manager that invoke initialization
     * @param m Instance of music that starts playing
     * @return true if error handled
     */
    public boolean onMusicError(AudioManager a, Music m);
}
