package com.starjdevs.pacrunner.core.audio;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract Audio Manager.
 * Extends this for use in game.
 *
 * @author Andy Tyurin
 */
public abstract class AbstractAudioManager implements AudioManager {

    private Map<String, Sound> m_aSounds = new HashMap<String, Sound>(); // sounds
    private Map<String, Music> m_aMusic = new HashMap<String, Music>(); // musics
    private Music mCurrentMusic;
    private SoundPool mSoundPool;
    private MediaPlayer mMediaPlayer;
    private int mSoundsNumToLoad = 0;
    private int mSoundsNumLoaded = 0;
    private int mSoundsNumFailLoaded = 0;
    private Context mContext;
    private SoundInit mSoundInit; // sound initialization component
    private MusicInit mMusicInit; // music initialization component

    public AbstractAudioManager(Context context) {
        mContext = context; // keep context
    }

    // Init sounds by creating SoundPool
    public void initSound() {
        mSoundPool = new SoundPool(20, android.media.AudioManager.STREAM_MUSIC, 0);
    }

    // Init music by creating MediaPlayer
    public void initMusic() {
        mMediaPlayer = new MediaPlayer();
    }

    // Run initialization callback of sounds
    @Override
    public void init(SoundInit soundInit) {
        mSoundInit = soundInit;
        initSound();
        soundInit.init(this, mContext);
    }

    // Run initialization callback of musics
    @Override
    public void init(MusicInit musicInit) {
        mMusicInit = musicInit;
        initMusic();
        musicInit.init(this, mContext);
    }

    // get SoundInit callback
    public SoundInit getSoundInit() {
        return mSoundInit;
    }

    // get MusicInit callback
    public MusicInit getMusicInit() {
        return mMusicInit;
    }

    @Override
    public Sound createSound(String fileName, String soundName, long offset, long length) {
        Sound sound = new Sound(this, mContext, mSoundPool);

        try {
            sound.load(fileName, offset, length);
            m_aSounds.put(soundName, sound);
            mSoundsNumToLoad++; // Increase to load counter
        } catch (IOException e) {
            Log.e("AudioManager/Sound", "Can't load sound " + fileName);
            m_aSounds.remove(soundName);
            sound = null;
        }

        return sound;
    }

    @Override
    public Sound createSound(String fileName, String soundName) {
        return createSound(fileName, soundName, 0, 0);
    }


    @Override
    public Sound getSound(String soundName) {
        return m_aSounds.get(soundName);
    }

    // Overloaded - We need this to avoid callback bug in sound while getting sound reference init.
    public Sound getSound(int soundId) {
        for (Map.Entry<String,Sound> entry : m_aSounds.entrySet()) {
            if(entry.getValue().getId()==soundId)
                return entry.getValue();
        }
        return null;
    }

    @Override
    public void removeSound(String soundName) {
        m_aSounds.get(soundName).unload();
        m_aSounds.remove(soundName);
    }

    @Override
    public void disposeSounds() {
        for (Map.Entry<String,Sound> entry : m_aSounds.entrySet()) {
            entry.getValue().unload();
        }
        m_aSounds.clear();
        mSoundPool.release(); // release all resources
    }

    @Override
    public int getSoundsNumToLoad() {
        return mSoundsNumToLoad;
    }

    @Override
    public int getSoundsNumLoaded() {
        return mSoundsNumLoaded;
    }

    @Override
    public int getSoundsNumFailLoaded() {
        return mSoundsNumFailLoaded;
    }

    // Increment sounds num loaded
    public void iSoundsNumLoaded() {
        mSoundsNumLoaded++;
    }

    // Increment sounds num fail loaded
    public void iSoundsNumFailLoaded() {
        mSoundsNumFailLoaded++;
    }

    @Override
    public Music createMusic(String fileName, String musicName) {
        Music music = new Music(this, mContext, mMediaPlayer, musicName);

        try {
            music.check(fileName);
            m_aMusic.put(musicName, music);
        } catch (IOException e) {
            Log.e("AudioManager/Music", "Can't load sound " + fileName);
            m_aMusic.remove(musicName);
            music = null;
        }

        return music;
    }

    @Override
    public Music getMusic(String musicName) {
        return m_aMusic.get(musicName);
    }

    // Overloaded - We need this to avoid callback bug in music while getting music reference init.
    public Music getMusic(int musicId) {
        for (Map.Entry<String,Music> entry : m_aMusic.entrySet()) {
            if(entry.getValue().getId()==musicId)
                return entry.getValue();
        }
        return null;
    }

    @Override
    public void removeMusic(String musicName) {
        m_aMusic.get(musicName).unload();
        m_aMusic.remove(musicName);
    }

    @Override
    public void disposeMusic() {
        for (Map.Entry<String,Music> entry : m_aMusic.entrySet()) {
            entry.getValue().unload();
        }
        m_aMusic.clear();
    }

    @Override
    public void setCurrentMusic(Music music) {
        mCurrentMusic = music;
    }

    @Override
    public void setCurrentMusic(String musicName) {
        mCurrentMusic = getMusic(musicName);
    }

    @Override
    public Music getCurrentMusic() {
        return mCurrentMusic;
    }

    @Override
    public void dispose() {
        disposeSounds();
        disposeMusic();
    }

    @Override
    public void pauseAll() {
        if (mMediaPlayer!=null) {
            mSoundPool.autoPause();
        } else {
            Log.e("AudioManager/Sounds", "Can't pause sounds");
        }

        if (mMediaPlayer!=null) {
            mCurrentMusic.pause();
        } else {
            Log.e("AudioManager/Music", "Can't resume pause musics");
        }
    }

    @Override
    public void resumeAll() {
        if (mSoundPool!=null) {
            mSoundPool.autoResume();
        } else {
            Log.e("AudioManager/Sounds", "Can't resume sounds");
        }

        if (mMediaPlayer!=null) {
            try {
                mCurrentMusic.play();
            } catch (IOException e) {
                Log.e("AudioManager/Music", "Can't resume current music");
            }
        }

    }

}
