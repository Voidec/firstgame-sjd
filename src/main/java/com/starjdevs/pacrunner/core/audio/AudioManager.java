package com.starjdevs.pacrunner.core.audio;

import java.io.IOException;

/**
 * Audio manager interface.
 * There an audio manager controller that have functions to manage our Sound(s) & Music(s) objects.
 * It's mean, that you can create a new instance of Sound (or music) and than manage their states.
 * For example, if we wanna load and play our sound, we make next steps below:
 * ---------------------------------------------------------------------------
 * AudioManager = new GameAudioManager(this); // GameAudioManager extends AbstractAudioManager implements AudioManager
 * // param "this" context of Activity, where sounds will be run
 *
 * gameAudioManager.init(new SoundInit() {
 *     @Override
 *     public void init(AudioManager a, Context c) {
 *         a.createSound("explosion.ogg", "explosion", c);
 *         a.setLoop(true);
 *     }
 *
 *     @Override
 *     public void onLoadComplete(AudioManager a, Sound s, boolean loadStatus) {
 *         if (loadStatus) {
 *             a.getSound("explosion").play();
 *         } else {
 *             // error message here
 *         }
 *     }
 * }
 *
 * In init method we are creating sounds and then override onLoadComplete method for make something
 * with sound instance if it will be loaded. We can check load status of sound (true if load success and false if not)
 *
 * @author Andy Tyurin
 */
public interface AudioManager {


    /**
     * --------------
     * INITIALIZATION
     * --------------
     */

    /**
     * Init method used for initial sound/music loading.
     * It need for filling initial data of current sound/music instances
     * to audio manager and for their future control.
     * @param soundInit implement for sound initialization.
     * @see com.starjdevs.pacrunner.core.audio.SoundInit
     */

    public void init(SoundInit soundInit);

    /**
     * Overloaded initialization method for music.
     * @param musicInit implement for music initialization.
     * @see com.starjdevs.pacrunner.core.audio.MusicInit
     */
    public void init(MusicInit musicInit);


    /**
     * -----------------
     * SOUNDS MANAGEMENT
     * -----------------
     */

    /**
     * Create instance of Sound & store it in array.
     * @param fileName name of file that holds in asset/sound directory
     * @param soundName the sound name id. For example: "explosion", "fire".
     * @param offset Offset of sound cutting (for example you wanna get one sound effect from big audio file)
     * @param length Length of sound to play
     * @return instance of Sound, that we can use (play, stop, pause)
     */
    public Sound createSound(String fileName, String soundName, long offset, long length);

    /**
     * Overloaded create sound method
     */
    public Sound createSound(String fileName, String soundName);

    /**
     * Get sound instance from array by sound name id
     * @param soundName sound name id.
     * @return instance of Sound.
     */
    public Sound getSound(String soundName);

    /**
     * Remove sound instance from array by sound name and send to garbage collector.
     * @param soundName sound name id
     */
    public void removeSound(String soundName);

    /**
     * Clear all sounds from array and send sound instances to garbage collector.
     * Release memory from sounds.
     */
    public void disposeSounds();

    /**
     * Get number of sounds for loading.
     * @return number of sounds for load
     */
    public int getSoundsNumToLoad();


    /**
     * Get number of sounds that already loaded
     * If sound broken or can't be loaded, it not will be considered in loaded counter.
     * @return number of sounds that already loaded
     */
    public int getSoundsNumLoaded();

    /**
     * Get number of sounds that failed loading
     * @return number of sounds that failed loading
     */
    public int getSoundsNumFailLoaded();


    /**
     * ----------------
     * MUSIC MANAGEMENT
     * ----------------
     */

    /**
     * Create instance of Music & store it in array.
     * @param fileName file name of Music to load, path assets/music
     * @param musicName music name id (key in map array)
     * @return instance of Music
     */
    public Music createMusic(String fileName, String musicName);

    /**
     * Get instance of Music from array
     * @param musicName music name id
     * @return instance of Music
     */
    public Music getMusic(String musicName);

    /**
     * Remove music from array & send instance to garbage collector
     * @param musicName music name id
     */
    public void removeMusic(String musicName);

    /**
     * Clear all musics from array and send music instances to garbage collector.
     * Release memory from musics.
     */
    public void disposeMusic();

    /**
     * Set music instance to be current. We need this because can play only one instance whenever musics we have.
     * It's mean that before you play music, you should make it current.
     * So if you have more than one music and will try to play one of them,
     * it automatically will be assigned as current. ( music.play() -> audioManager.setCurrent(this) )
     * @param music music instance
     */
    public void setCurrentMusic(Music music);

    /**
     * Overloaded, set music instance to be current.
     * @param musicName music name id
     */
    public void setCurrentMusic(String musicName);

    /**
     * Get current instance of music, that's can be used for future manage.
     * @return instance of music
     */
    public Music getCurrentMusic();


    /**
     * ------
     * COMMON
     * ------
     */

    /**
     * Fully dispose (clear) all resources of audio manager.
     * Use it when game come to end.
     * This method run to dispose methods of sound & music.
     * disposeSounds(); disposeMusics();
     */
    public void dispose();

    /**
     * Pause all sounds & musics.
     * Useful when we wanna pause activity.
     */
    public void pauseAll();

    /**
     * Resume all sounds & musics.
     * Useful when we wanna resume activity.
     */
    public void resumeAll();
}
