package com.starjdevs.pacrunner.core.audio;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.SoundPool;
import java.io.FileDescriptor;
import java.io.IOException;

/**
 * Sound instance that we can create by AudioManager and then control it.
 *
 * @author Andy Tyurin
 */
public class Sound {
    private AssetManager mAssetManager;
    private SoundPool mSoundPool;
    private Context mContext;
    private AbstractAudioManager mAudioManager;
    private int mSoundId = -1;
    private boolean mLoop = false;
    private int mSoundStreamId;
    private float mLeftVolume;
    private float mRightVolume;
    private String mFileName;

    /**
     * Init Sound data & create instance
     * @param audioManager AudioManager that try to create Sound
     * @param c Context where Sound must be played
     * @param soundPool SoundPool component from AudioManager for sound management
     */
    public Sound(AudioManager audioManager, Context c, SoundPool soundPool) {
        mAssetManager = c.getAssets();
        mSoundPool = soundPool;
        mAudioManager = (AbstractAudioManager) audioManager;
        mContext = c;
    }

    /**
     * Load sound via asset file descriptor
     * @param fileName name of a sound file to load
     * @param offset Offset from left of a sound
     * @param length Offset from right of a sound
     * @throws IOException Throws if can't load file
     */
    public void load(String fileName, long offset, long length) throws IOException {
        mFileName = fileName;
        AssetFileDescriptor afd = mAssetManager.openFd("sounds/" + fileName);
        FileDescriptor fd = afd.getFileDescriptor();

        // If offset & length less or equal zero, set default values via asset file descriptor
        if (offset<=0)
            offset=afd.getStartOffset();
        if (length<=0)
            length=afd.getLength();

        mSoundId = mSoundPool.load(fd, offset, length, 1);
        afd.close();

        // set listener, which fire when on load complete & redirect to Init method of AudioManager
        mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                // If load success, increment sounds number loaded counter
                if (status==0) {
                    mAudioManager.iSoundsNumLoaded();
                } else {
                    mAudioManager.iSoundsNumFailLoaded();
                }
                // Redirect onLoadComplete to AudioManager's init method
                mAudioManager.getSoundInit().onLoadComplete(mAudioManager, mAudioManager.getSound(sampleId), status==0);
            }
        });
    }

    /**
     * Get id of current Sound
     * @return sound id that was returned by SoundPool
     */
    public int getId() {
        return mSoundId;
    }


    /**
     * Unload data from SoundPool component of current Sound
     * @return true if unload complete
     */
    public boolean unload() {
        return mSoundPool.unload(mSoundId);
    }


    /**
     * Play current Sound.
     * We can play only loaded sound!
     * @param leftVolume left stream volume from 0.0f - 1.0f
     * @param rightVolume right stream volume from 0.0f - 1.0f
     * @param looped is looped stream? true/false
     */
    public void play(float leftVolume, float rightVolume, boolean looped) {
        setVolume(leftVolume, rightVolume); // set volume
        int loop = setLoop(looped); // set loop

        // if sound has soundId try to play
        if (mSoundId!=-1) {
            mSoundStreamId = mSoundPool.play(mSoundId, mLeftVolume, mRightVolume, 1, loop, 1);
        }
    }

    // Overloaded play
    public void play(float leftVolume, float rightVolume) {
        play(leftVolume, rightVolume, false);
    }

    // Overloaded play
    public void play() {
        play(-1,-1,false);
    }

    /**
     * Set volume of sound.
     * @param leftVolume left stream volume from 0.0f - 1.0f
     * @param rightVolume right stream volume from 0.0f - 1.0f
     */
    public void setVolume(float leftVolume, float rightVolume) {
        if (!(leftVolume<=0.0f && leftVolume>=1.0f && rightVolume<=0.0f && rightVolume>=1.0f)) {
            mLeftVolume=leftVolume;
            mRightVolume=rightVolume;
        } else {
            android.media.AudioManager au = (android.media.AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
            float actualVolume = au.getStreamVolume(android.media.AudioManager.STREAM_MUSIC);
            float maxVolume = au.getStreamMaxVolume(android.media.AudioManager.STREAM_MUSIC);
            float volume = actualVolume / maxVolume;
            mLeftVolume = mRightVolume = volume;
        }
        mSoundPool.setVolume(mSoundId, leftVolume, rightVolume);
    }

    /**
     * Get current loop status
     * @return true if looped
     */
    public boolean getLoop() {
        return mLoop;
    }

    /**
     * Set loop
     * @param looped if true, sound to be looped
     * @return -1 if looped, 0 if not
     */
    public int setLoop(boolean looped) {
        mLoop = looped;
        int isLooped=0;
        if (mLoop)
            isLooped=-1;
        mSoundPool.setLoop(mSoundStreamId, isLooped);

        return isLooped;
    }

    /**
     * Pause sound
     */
    public void pause() {
        // Pause if we have sound stream returned by play
        if (mSoundStreamId!=0) {
            mSoundPool.pause(mSoundStreamId);
        }
    }

    /**
     * Stop sound
     */
    public void stop() {
        // Stop if we have sound stream returned by play
        if (mSoundStreamId!=0) {
            mSoundPool.stop(mSoundStreamId);
        }
    }

    /**
     * Get file name of current sound
     * @return file name of current sounds
     */
    public String getFileName() {
        return mFileName;
    }

}
