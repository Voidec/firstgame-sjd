package com.starjdevs.pacrunner.core.audio;

import android.content.Context;

/**
 * We realize this interface in method init of AudioManager.
 * @see com.starjdevs.pacrunner.core.audio.AudioManager
 * It's need for binding our sounds data with AudioManager and their future management.
 *
 * @author Andy Tyurin
 */
public interface SoundInit {

    /**
     * Init sounds by creating them inside method.
     * It need for filling initial data of sounds to audio manager and for their future control.
     * @param a audio manager that invoke initialization
     * @param c context that run our sounds (activity)
     */
    public void init(AudioManager a, Context c);

    /**
     * This method firing every sound load process complete (no matter that is success).
     * If loadStatus is true, that mean that loading of current sound ends with success.
     * We can process load error by checking loadStatus (false)
     * @param a audio manager that invoke initialization
     * @param s current sound that completed his load.
     * @param loadStatus boolean of current success status. True if load complete success.
     */
    public void onLoadComplete(AudioManager a, Sound s, boolean loadStatus);

}
