package com.starjdevs.pacrunner.core.audio;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.FileDescriptor;
import java.io.IOException;

/**
 * @author Andy Tyurin
 */
public class Music {

    private AudioManager mAudioManager;
    private AssetManager mAssetManager;
    private MediaPlayer mMediaPlayer;
    private String mFileName;
    private String mMusicName;
    private int mMusicId = 0;
    private boolean mLoop = false;
    private boolean mPaused = false;
    private int mLoopStage = 0;
    private long mOffset;
    private long mLength;

    /**
     * Create Music instance from AudioManager for fill initial data.
     * @param audioManager AudioManager that try to create Sound
     * @param c Context where Sound must be played
     * @param mediaPlayer MediaPlayer component from AudioManager for music management
     */
    public Music(AudioManager audioManager, Context c, MediaPlayer mediaPlayer, String musicName) {
        mAssetManager = c.getAssets();
        mAudioManager = audioManager;
        mMediaPlayer = mediaPlayer;
        mMusicName = musicName;
    }

    /**
     * Check, is file exists.
     * @param fileName Name of music file
     * @throws IOException Throws if file not found
     */
    public void check(String fileName) throws IOException {
        AssetFileDescriptor afd = mAssetManager.openFd("music/"+fileName);
        if (afd.getFileDescriptor().valid())
            mAudioManager.setCurrentMusic(this); // set to be current
        mFileName = fileName; // save file name to field
        afd.close();
    }


    public void play(long offset, long length) throws IOException {
        Music previousMusic = null;

            previousMusic = mAudioManager.getCurrentMusic();
            mOffset=previousMusic.getOffset();
            mLength=previousMusic.getLength();

            if (previousMusic.getName().equals(this.getName())) {
                if (isPaused()) {
                    mMediaPlayer.start();
                    setPaused(false);
                } else {
                    load(mFileName, mOffset, mLength);
                }

            } else {
                load(mFileName, offset, length);
            }

        // set on music prepared listener
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.start();
                setPaused(false);
            }
        });

        // set on completion listener
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Music currentMusic = ((AbstractAudioManager) mAudioManager).getMusic(mediaPlayer.getAudioSessionId());
                MusicInit init = ((AbstractAudioManager) mAudioManager).getMusicInit();
                init.onMusicComplete(mAudioManager, currentMusic, ++mLoopStage);
                if (currentMusic.isLooped())
                    try {
                        currentMusic.stop();
                        currentMusic.play();
                    } catch (IOException e) {
                        Log.e("AudioManager/Music", "Error occured while trying to play looped music");
                    }
            }
        });

        // set on error listener
        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                Music currentMusic = ((AbstractAudioManager) mAudioManager).getMusic(mediaPlayer.getAudioSessionId());
                MusicInit init = ((AbstractAudioManager) mAudioManager).getMusicInit();
                return init.onMusicError(mAudioManager, currentMusic);
            }
        });
    }

    // Overloaded play method
    public void play() throws IOException {
        play(-1, -1);
    }

    // Load music for play
    public void load(String fileName, long offset, long length) throws IOException{
        mMediaPlayer.reset();
        AssetFileDescriptor afd = mAssetManager.openFd("music/" + fileName);
        FileDescriptor fd = afd.getFileDescriptor();

        if (offset<=0)
            mOffset=afd.getStartOffset();
        if (length<=0)
            mLength=afd.getLength();

        mMediaPlayer.setDataSource(fd, mOffset, mLength);
        mMediaPlayer.setAudioStreamType(android.media.AudioManager.STREAM_MUSIC);
        mMusicId = mMediaPlayer.getAudioSessionId();
        mAudioManager.setCurrentMusic(this); // set music to be current
        mMediaPlayer.prepareAsync();
        afd.close();
    }

    /**
     * Set sound to be paused
     * @param paused true if pause
     */
    private void setPaused(boolean paused) {
        mPaused = paused;
    }

    /**
     * Get Music pause status
     * @return true if paused
     */
    private boolean isPaused() {
        return mPaused;
    }

    /**
     * Set music to be looped
     * @param looped true if looped
     */
    public void setLoop(boolean looped) {
        mLoop=looped;
    }

    /**
     * Get Music's loop status
     * @return true if looped
     */
    public boolean isLooped() {
        return mLoop;
    }

    /**
     * Get id of Music
     * @return music id
     */
    public int getId() {
        return mMusicId;
    }

    /**
     * Pause Music
     */
    public void pause() {
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
            setPaused(true);
        }
    }

    /**
     * Stop music. Move to start point playing.
     */
    public void stop() {
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            setPaused(false);
        }
    }

    /**
     * Unload music & release resources of MusicPlayer
     */
    public void unload() {
        mMediaPlayer.reset();
        mMediaPlayer.release();
    }

    /**
     * Get music name id
     * @return music name id
     */
    public String getName() {
        return mMusicName;
    }

    /**
     * Get music file name
     * @return music file name
     */
    public String getFileName() {
        return mFileName;
    }

    /**
     * Get music offset
     * @return music offset
     */
    public long getOffset() {
        return mOffset;
    }

    /**
     * Get music length
     * @return music length
     */
    public long getLength() {
        return mLength;
    }
}
