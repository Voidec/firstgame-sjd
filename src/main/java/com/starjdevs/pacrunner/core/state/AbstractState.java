package com.starjdevs.pacrunner.core.state;

/**
 * AbstractState.
 * Has main logic of a state that was implemented by State interface.
 *
 * @author Andy Tyurin
 */
public abstract class AbstractState implements State {

    private String mStatePath;
    private String mChildPath;
    private StateManager mStateManager;


    @Override
    public void onRemove(StateManager m) {
        /**
         * It not will be required in every state,
         * only if u need to dispose resources when going to another state
         */
    }

    @Override
    public void setDefaultChildPath(String statePath) {
        mChildPath=statePath;
    }

    @Override
    public String getDefaultChildPath() {
        return mChildPath;
    }

    @Override
    public boolean hasDefaultChild() {
        return mChildPath!=null;
    }

    /**
     * Set path of a state
     * @param statePath state path
     */
    public void setPath(String statePath) {
        mStatePath = statePath;
    }

    /**
     * Get path of a state
     * @return state path
     */
    public String getPath() {
        return mStatePath;
    }

    /**
     * Set holder - state manager of a state
     * @param m Instance of state manager
     */
    public void setStateManager(StateManager m) {
        mStateManager=m;
    }

    /**
     * Get holder - state manager of a state
     * @return Intance of state manager
     */
    public StateManager getStateManager() {
        return mStateManager;
    }


}
