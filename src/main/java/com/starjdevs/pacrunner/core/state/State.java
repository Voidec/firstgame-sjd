package com.starjdevs.pacrunner.core.state;

/**
 * State interface.
 * Main logic of a State.
 *
 * @author Andy Tyurin
 */
public interface State {
    /**
     * Fire when state starts building
     * @param m home state manager
     */
    public void onCreate(StateManager m);

    /**
     * Fire when state removes from active state
     * @param m home state manager
     */
    public void onRemove(StateManager m);

    /**
     * Set default child state path
     * @param statePath absolute path of default child state
     */
    public void setDefaultChildPath(String statePath);

    /**
     * Get default child state path
     * @return default child state path
     */
    public String getDefaultChildPath();

    /**
     * Does state has a default child?
     * @return true if it has
     */
    public boolean hasDefaultChild();

    /**
     * Get path of a state
     * @return state path
     */
    public String getPath();
}
