package com.starjdevs.pacrunner.core.state;

import android.app.Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * AbstractStateManager, abstract class.
 * StateManager can holds all states that you can create by extending AbstractState class.
 * You can manage states, set active state, remove, add new etc.
 * StateManager need for good practice of code routing. It's mean that you can divide your code for states, that
 * can be set as active state and run.
 *
 * For example you can make two states of your game:
 * - INTRO
 * - GAME
 * You can run intro state and his code by function below:
 * > mStateManager.set("INTRO"):
 *
 * or set GAME as active state and run too:
 * > mStateManager.set("GAME"):
 *
 * Remember, that you can attach default child state to parent-state.
 * It's useful if you wanna make your program to be hierarchical
 *
 * @author Andy Tyurin
 */
public class AbstractStateManager implements StateManager {
    Map<String, State> m_aStates = new HashMap<String, State>(); // hash with state path as key and state instance
    String mActiveStatePath=""; // active state path
    ArrayList<OnStateChangeListener> onStateChangeListeners = new ArrayList<OnStateChangeListener>(); // onStateChange listeners
    Activity mActivity; // Activity where manager will be work

    // require to be defined in a state
    public AbstractStateManager(Activity a) {
        mActivity = a;
    }

    @Override
    public Activity getActivity() {
        return mActivity;
    }

    @Override
    public void add(String statePath, State state) throws StateException {
        String[] pathParts = statePath.toUpperCase().split("\\.");  // holds split path

        // State path can't be empty
        if (statePath.equals("")) {
            throw new StateException("State must to have path");
        }

        // State path can be added if previous state of a last state is exists or if it is global state (first state)
        if (pathParts.length!=1 && !m_aStates.containsKey(pathParts[pathParts.length-2]))
            throw new StateException("You can't add a state at current path, because don't define a parent state!");

        // Hold state in array
        m_aStates.put(statePath, state);
        ((AbstractState) state).setPath(statePath);
        ((AbstractState) state).setStateManager(this);
    }

    @Override
    public State get(String statePath) {
        return m_aStates.get(statePath);
    }

    @Override
    public State get() {
        return get(mActiveStatePath); // return current active state
    }

    @Override
    public void remove(String statePath) {
        m_aStates.remove(statePath);
    }

    @Override
    public void set(String nPath) throws StateException {
        String[] a_nPath = nPath.split("\\."); // keep elements of new path
        String[] a_cPath = findCommonPath(mActiveStatePath, nPath).split("\\."); // keep elements of common path

        // state can be already added at new path
        if (!m_aStates.containsKey(nPath)) {
            throw new StateException("Can't set state at path:" + nPath + "\nState not found");
        } else if (!nPath.equals(mActiveStatePath)) {

            /**
             * Remove while:
             * - Active state path not to be equal empty
             * - Active state path not to be equal common path
             * - Active state path not to be equal new path
             * Break if we haven't a common path AND active path
             * has only one element or new path has more elements then common path
             */
            String tmpActivePath=mActiveStatePath;
            while (!mActiveStatePath.equals("")
                    && !mActiveStatePath.equals(findCommonPath(mActiveStatePath, nPath))
                    && !mActiveStatePath.equals(nPath)) {

                // invoke remove method
                get(mActiveStatePath).onRemove(this);
                mActiveStatePath=mActiveStatePath.replaceAll("\\..*", "");

                // break or set active path to be empty
                if (!mActiveStatePath.contains(".") || a_nPath.length>a_cPath.length) {
                    if (!a_cPath[0].equals(""))
                        break;

                    // if at start of removing active path had not only one element then try to remove
                    if (tmpActivePath.contains("."))
                        get(mActiveStatePath).onRemove(this);
                    mActiveStatePath="";
                }


            }

            // redefine common path (can be new active path)
            a_cPath = findCommonPath(mActiveStatePath, nPath).split("\\.");

            // create a new state while active state to be equal new path
            for (int i=0; !mActiveStatePath.equals(nPath); i++) {

                // skip common path if we have it
                if (i<=(a_cPath.length-1)) {
                    if (!a_cPath[i].equals("") && a_cPath[i].equals(mActiveStatePath)) {
                        if (mActiveStatePath.equals(""))
                            mActiveStatePath = a_cPath[i];
                        continue;
                    }
                }

                // add path element to active path
                if (a_nPath.length==1) {
                    mActiveStatePath = nPath;
                } else {
                    if (mActiveStatePath.equals(""))
                        mActiveStatePath = a_nPath[i];
                    else {
                        mActiveStatePath += "." + a_nPath[i];
                    }
                }

                // if there is last element, invoke listeners
                if (mActiveStatePath.equals(nPath)) {

                    if (!get(mActiveStatePath).hasDefaultChild()) {

                        // run listener's events
                        for (OnStateChangeListener listener : onStateChangeListeners) {
                            listener.onStateChange(this, get(mActiveStatePath));
                        }
                    }

                    get(mActiveStatePath).onCreate(this); // run create method of last element
                    break;
                }

                // invoke create method
                get(mActiveStatePath).onCreate(this);

            }

            // run child if has
            if (get(mActiveStatePath).hasDefaultChild())
                set(get(mActiveStatePath).getDefaultChildPath());

        }
    }

    @Override
    public void setOnStateChangeListener(OnStateChangeListener listener) {
        onStateChangeListeners.add(listener);
    }

    /**
     * Get common path between new and old
     * @param oPath old path
     * @param nPath new path
     * @return common path
     */
    public String findCommonPath(String oPath, String nPath) {
        String[] a_oPath = oPath.split("\\."); // old path
        String[] a_nPath = nPath.split("\\."); // new path
        String cPath = ""; // common path
        if (!(oPath.equals("") && nPath.equals(""))) {
            for (int i = 0; i < Math.min(a_nPath.length, a_oPath.length); i++) {
                if (a_nPath[i].equals(a_oPath[i])) {
                    if (cPath.equals(""))
                        cPath = a_nPath[i];
                    else
                        cPath += "." + a_oPath[i];
                } else {
                    break;
                }
            }
        }
        return cPath;
    }



}
