package com.starjdevs.pacrunner.core.state;

import android.app.Activity;

/**
 * StateManager interface.
 * Define functional of State manager.
 * State managers used from states manipulation (add,remove, set current and etc.)
 *
 * @author Andy Tyurin
 */
public interface StateManager{

    /**
     * Add a new state to route statePath.
     * For example: add("INIT.LOADING");
     * @param statePath route patch of a state
     * @param state instance of state (strategy)
     */
    public void add(String statePath, State state) throws StateException;

    /**
     * Get instance of State
     * @param statePath route path of a state
     * @return state instance
     */
    public State get(String statePath);

    /**
     * Get current active State
     * @return state instance
     */
    public State get();

    /**
     * Remove state instance
     * @param statePath route path of a state
     */
    public void remove(String statePath);

    /**
     * Set current state.
     * @param statePath route patch of a state
     */
    public void set(String statePath) throws StateException;

    /**
     * Set on state change listener
     * Will fire when state to be changed
     * @param listener Listener instance
     * @see com.starjdevs.pacrunner.core.state.OnStateChangeListener
     */
    public void setOnStateChangeListener(OnStateChangeListener listener);

    /**
     * Get activity
     * @return android activity
     */
    public Activity getActivity();

}

