package com.starjdevs.pacrunner.core.state;

import com.starjdevs.pacrunner.game.states.GameState;
import com.starjdevs.pacrunner.game.states.IntroLoaderState;
import com.starjdevs.pacrunner.game.states.IntroState;

/**
 * StateConstants abstract class.
 * There is constant holder. You can hold your static states here and use in projects.
 * You can add another constants whenever you like.
 *
 * @author Andy Tyurin
 */
public abstract class StateConstants {

    public static final State STATE_INTRO = new IntroState();
    public static final State STATE_INTRO_LOADER = new IntroLoaderState();
    public static final State STATE_GAME = new GameState();

}