package com.starjdevs.pacrunner.core.state;

/**
 * StateException.
 * Occur if error detected in state
 * Extends custom Exception.
 *
 * @author Andy Tyurin
 */
public class StateException extends Exception {

    public StateException(String message) {
        super(message);
    }

    public StateException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
