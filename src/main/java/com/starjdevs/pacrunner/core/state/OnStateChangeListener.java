package com.starjdevs.pacrunner.core.state;

/**
 * OnStateChangeListener interface.
 * Fires when current state in state manager was changed by another.
 * @see com.starjdevs.pacrunner.core.state.StateManager
 *
 * @author Andy Tyurin
 */
public interface OnStateChangeListener {
    /**
     * Fires when active state was changed to another
     * @param m State manager, holder of this listener
     * @param s state
     */
    public void onStateChange(StateManager m, State s);
}
