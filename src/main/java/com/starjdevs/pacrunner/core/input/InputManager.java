package com.starjdevs.pacrunner.core.input;

import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;
import com.starjdevs.pacrunner.core.logic.Vector2f;

/**
 * Created by Void on 17.09.2014.
 */
public interface InputManager {
    /**
     * The main input logic, created in SurfaceView onTouchEvent method, example:
     *  public boolean onTouchEvent(MotionEvent event) {
     *      inputManager.onTouch(this, event);
     *      return true; //processed
     * }
     *
     * @param view  - SurfaceView
     * @param event - MotionEvent from onTouchEvent method
     * @return false, its the last input listener
     */
    public boolean onTouch(View view, MotionEvent event);

    /**
     * with this method you can check if the user touched something at the screen

     *
     * @param r - Rectangle, at what position you are waiting for user input
     * @return true if player touched it, false if not
     */

    public boolean touch(RectF r);

    /**
     *
     * same as    public boolean touch(RectF r), but you give x1, y1, x2, y2 position of rectangle, where we wait for touch
     *
     * @param x1 - left top corner
     * @param y1 - left bot corner
     * @param x2 - right rop corner
     * @param y2 - right left corner
     * @return true if player touched it, false if not
     */
    public boolean touch(float x1, float y1, float x2, float y2);

    /**
     * set the control area center, need it to understand which way player want to move/shoot
     *
     * @param moveCenter - vector2f of the center
     */
    public void setRefPoint(Vector2f moveCenter);

    /**
     *
     * set the control area, where should user touch the screen to move in game
     *
     * @param rectF - rectangle of control area
     */
    public void setControlArea(RectF rectF);

    /**
     *
     * return the normalised vector of user touch, directional vector, where should player move/shoot
     *
     * @return
     */
    public Vector2f getNorm();
}