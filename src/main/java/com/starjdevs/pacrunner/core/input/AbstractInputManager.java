package com.starjdevs.pacrunner.core.input;

import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;
import com.starjdevs.pacrunner.core.logic.Vector2f;

/**
 * Created by Void on 17.09.2014.
 */
public abstract class AbstractInputManager implements View.OnTouchListener, InputManager{
    private boolean touch1 = false;
    private float x, y, a, b;
    private Vector2f center, touch, sub, norm = new Vector2f(0,0);
    private RectF moveRect;

    @Override
    public boolean onTouch(View view, MotionEvent event) {
//        // событие
//        int actionMask = event.getActionMasked();
//        // индекс касания
//        int pointerIndex = event.getActionIndex();
//        // число касаний
//        int pointerCount = event.getPointerCount();
//        //взять действие по определенному id
//        int pointerID = event.getPointerId(i);

        int action = event.getAction();
        switch(action){
            case MotionEvent.ACTION_POINTER_2_UP:
                a = 0;
                b = 0;
                break;
            case MotionEvent.ACTION_DOWN:
                touch1 = false;
                x = event.getX(event.getPointerId(0));
                y = event.getY(event.getPointerId(0));
                touch1 = true;
                if (moveRect.contains((int) event.getX(), (int) event.getY())) {
                    touch = new Vector2f(event.getX(), event.getY());
                    sub = touch.sub(center);
                    norm = sub.normalize();
                }
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                touch1 = false;
                for (int i = 0; i < event.getPointerCount(); i++) {
                    x = event.getX(i);
                    y = event.getY(i);
                    touch1 = true;
                    if (moveRect.contains((int) event.getX(event.getPointerId(i)), (int) event.getY(event.getPointerId(i)))) {
                        touch = new Vector2f( event.getX(event.getPointerId(i)), event.getY(event.getPointerId(i)));
                        sub = touch.sub(center);
                        norm = sub.normalize();
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                touch1 = false;
                if(event.getPointerCount() > 1){
                    for (int i = 1; i < event.getPointerCount(); i++) {
                        a = event.getX(event.getPointerId(i));
                        b = event.getY(event.getPointerId(i));
                        touch1 = true;
                        if (moveRect.contains((int) event.getX(i), (int) event.getY(i))) {
                            touch = new Vector2f(event.getX(i), event.getY(i));
                            sub = touch.sub(center);
                            norm = sub.normalize();
                        }
                    }
                }
                x = event.getX(event.getPointerId(0));
                y = event.getY(event.getPointerId(0));

                touch1 = true;
                if(moveRect.contains((int)event.getX(event.getPointerId(0)), (int)event.getY(event.getPointerId(0)))){
                    touch = new Vector2f(event.getX(event.getPointerId(0)), event.getY(event.getPointerId(0)));
                    sub = touch.sub(center);
                    norm = sub.normalize();
                }
                break;
            case MotionEvent.ACTION_POINTER_UP:

                break;

            case MotionEvent.ACTION_UP:
                x = 0;
                y = 0;
                a = 0;
                b = 0;
                touch1 = false;
                break;
            default:
        }
        return false; //processed
    }
    public boolean touch(RectF r){
        if (touch1)
            return r.contains(x, y) || r.contains(a, b);
        else return false;
    }
    public boolean touch(float a, float b, float x, float y){
        RectF r = new RectF(a, b, x, y);
        if (touch1)
        {return r.contains(this.x, this.y) || r.contains(a, b); }
        else return false;
    }
    public void setRefPoint(Vector2f moveCenter){
        this.center = moveCenter;
    } //set the center of movement rectangle;
    public void setControlArea(RectF rectF){
        this.moveRect = rectF;
    }
    public Vector2f getNorm(){
        return norm;
    }
}