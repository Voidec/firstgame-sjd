package com.starjdevs.pacrunner.core.file;

/**
 * Created by Void on 17.09.2014.
 */

import android.os.Environment;

import java.io.*;

public abstract class  AbstractFileManager implements FileManager{
    public void write(String path, String fileName){
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File (sdCard.getAbsolutePath() + path); //"/dir1/dir2"
        dir.mkdirs();
        File file = new File(dir, fileName); // + ".txt"

        try {
            FileOutputStream f = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void load(String path, String fileName){
        File sdCard = Environment.getExternalStorageDirectory();
        File file = new File(sdCard.getAbsolutePath() + path, fileName); //"/dir1/dir2","FILE.txt"
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                System.out.println(line);
                text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            System.out.println("true");
            return true;
        }
        System.out.println("false");
        return false;
    }

    public void delete( String path ,String file){
        File sdCard = Environment.getExternalStorageDirectory();

        File fileDel = new File(sdCard.getAbsolutePath() + path, file);
        boolean deleted = fileDel.delete();
    }
}