package com.starjdevs.pacrunner.core.file;

/**
 * @author Andy Tyurin
 */
public interface FileManager {
    /**
     * write information to the file, will create a file if there's no one;
     *
     * @param path - path to the directory, example: "/dir1/dir2"
     * @param fileName - name of the file to save info, example: "FILE.txt"
     */
    public void write(String path, String fileName);

    /**
     * read information from the file, if one exist
     *
     * @param path - path to the directory, example: "/dir1/dir2"
     * @param fileName - name of the file to load info from, example: "FILE.txt"
     */
    public void load(String path, String fileName);

    /**
     * check if SD Card or other External Storage is open and accessible to read/write;
     *
     * @return true if its open, false if not
     */
    public boolean isExternalStorageWritable();

    /**
     *
     * delete file from External Storage
     *
     * @param path - path to the directory, example: "/dir1/dir2"
     * @param file - name of the file to delete, example: "FILE.txt"
     */
    public void delete( String path ,String file);

}
