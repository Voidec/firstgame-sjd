package com.starjdevs.pacrunner.core.graphics;

import android.graphics.Bitmap;

/**
 * Created by Void on 11.09.2014.
 */
interface BitmapManagerInterface {

    /**
     * load an image from assets and put it into BitmapManager array;
     *
     * @param name - name of the future image
     * @param path - name of the image in assets/images(without .png, if image has name image.png, you just write "image"
     * @param config - set the format for the image (ARGB_4444/ARGB_8888/etc.)
     */
    public void load(String name, String path, Bitmap.Config config);

    /**
     * set the format for the image (ARGB_4444/ARGB_8888/etc.)
     *
     * @param bitmap - take the loaded image from the array
     * @param config -  set the format for the image (ARGB_4444/ARGB_8888/etc.)
     * @return image with new config
     */
    public Bitmap setFormat(Bitmap bitmap, Bitmap.Config config);

    /**
     * delete the image from the array to free the memory
     *
     * @param name - name of the image in array
     */
    public void clear(String name);

    /**
     * delete all images from bitmap array
     *
     */
    public void clearAll();

    /**
     * get the image from array(can be given to another Bitmap to simplify)
     *
     * @param name - name of the image in array
     * @return asked image
     */
    public Bitmap get(String name);

    /**
     * clear the animation from the animations array
     *
     * @param name - name of the image in array
     */
    public void clearAnimation(String name);

    /**
     * delete all animations from the array
     *
     */
    public void clearAllAnimation();

    /**
     * get the animation, must be in entity update to change the picture
     *
     * @param name - name of the animation in array
     * @return asked animation
     */
    public Bitmap getAnimation(String name);

    /**
     * load an animation list to the array - give it a name, a bitmap
     * from where to load and a config;
     *
     * @param name - name of the future animation in array
     * @param bmp - loaded bitmap with animation pictures
     * @param config - set the format for the image (ARGB_4444/ARGB_8888/etc.)
     */
    public void loadAnimation(String name, Bitmap bmp, Bitmap.Config config);

    /**
     * crop this bitmap to make the animation give it a stating x and y of the picture,
     * width and height of the future animation and how many pictures to cut
     *
     * IMAGES MUST BE IN A ROW
     *
     * @param name - name of the loaded animation
     * @param startX - start x point in the Bitmap
     * @param startY - start Y point in the Bitmap
     * @param width - width of each animation picture
     * @param height - height of each animation picture
     * @param frameCount - quantity of images in a row
     */
    public void cropAnimation(String name, int startX, int startY, int width, int height, int frameCount);

    /**
     * give the image a new size - height and width
     *
     * @param name - name of the loaded image
     * @param newWidth - new width of the image
     * @param newHeight - new height of the image
     */
    public void resizeImage(String name, int newWidth, int newHeight);

    /**
     *  cut the image from a sprite sheet
     *
     * @param newName - name of the new image
     * @param name - name of the sprite sheet in the array
     * @param startX - start - start x point in the Bitmap
     * @param startY - start Y point in the Bitmap
     * @param newWidth - width of the image
     * @param newHeight - height of the image
     */
    public void cutImage(String newName, String name, int startX, int startY, int newWidth, int newHeight);

    /**
     * resize animation to the new width and height
     *
     * @param name - name of the new image
     * @param newWidth - new width of the animation
     * @param newHeight - new height of the animation
     */
    public void resizeAnimation(String name, int newWidth, int newHeight);

    /**
     * create new Bitmap with width and height of new map
     * @param name - name of the new Bitmap
     * @param x - how many tiles will be from left to right
     * @param y - how many tiles will be from top to down
     * @param width - width of each tile
     * @param height - height of each tile
     * @param conf - set the format for the image (ARGB_4444/ARGB_8888/etc.)
     */
    public void createMap(String name, int x, int y, int width, int height, Bitmap.Config conf);

    /**
     * Add array of tiles and draw them to the map
     * @param name - name of the bitmap that will be our level
     * @param tiles - array of tiles
     * @param x - how many tiles will be from left to right
     * @param y - - how many tiles will be from top to down
     */
    public void addTiles(String name, Tile[][] tiles, int x, int y);

}