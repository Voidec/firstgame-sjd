package com.starjdevs.pacrunner.core.graphics;

import android.graphics.*;

import java.util.ArrayList;

/**
 * Created by Void on 12.09.2014.
 */
public class AnimatedBitmap {
    private final Bitmap bitmap;
    private ArrayList<Bitmap> list;
    Bitmap.Config config;
    private long lastTime = 0;
    private int fps = 1;
    private int framePeriod = 1000/fps;
    private int currentFrame = 0;
    private Rect newR, oldR;
    public AnimatedBitmap(Bitmap bmp, Bitmap.Config config){
        this.bitmap = bmp;
        this.config = config;
    }
    public void crop(int startX, int startY, int width, int height, int framesCount){
        list = new ArrayList<Bitmap>(framesCount);
        for(int i =0; i < framesCount; i++) {
            Bitmap bmpNew = Bitmap.createBitmap(this.bitmap, startX + width*i, startY, width, height);
            list.add(bmpNew);
        }
        fps = framesCount;
    }
    public Bitmap get(){
        if(System.currentTimeMillis() > lastTime + framePeriod){
            lastTime = System.currentTimeMillis();
            currentFrame++;
            if(currentFrame > fps-1)
                currentFrame = 0;
            return list.get(currentFrame);
        } else {
            return list.get(currentFrame);
        }
    }
    public AnimatedBitmap resize(int newWidth, int newHeight){
        ArrayList<Bitmap> list2 = new ArrayList<Bitmap>(list.size());
        Bitmap bmp;
        for(int i = 0; i < list.size(); i++){
            newR = new Rect(0,0,newWidth, newHeight);
            bmp = list.get(i);
            oldR = new Rect(0,0,bmp.getWidth(),bmp.getHeight());
            Bitmap convertedBitmap = Bitmap.createBitmap(newWidth, newHeight, bmp.getConfig());
            Canvas canvas = new Canvas(convertedBitmap);
            Paint paint = new Paint();
            paint.setColor(Color.BLACK);
            canvas.drawBitmap(bmp, oldR, newR, paint);
            list2.add(convertedBitmap);
        }
        list.clear();
        list.addAll(list2);
        return this;
    }
}
