package com.starjdevs.pacrunner.core.graphics;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;

/**
 * Created by Void on 20.09.2014.
 */
public class Tile {
    Bitmap tile;
    private int x, y;
    private boolean collision;
    private RectF rect;
    Tile(Bitmap bmp) {
        this.tile = bmp;

    }
    public void setCoords(int x, int y){
        this.x = x;
        this.y = y;
        rect = new RectF(x ,y ,x + tile.getWidth(), y + tile.getHeight());
    }
    public void draw(Canvas canvas){
        canvas.drawBitmap(tile, x, y, null);
    }
    public Bitmap getTile(){
        return tile;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public void setCollision(boolean b){
        collision = b;
    }
    public boolean getCollison(){
        return collision;
    }
    public RectF getTileRect(){
        return rect;
    }


}