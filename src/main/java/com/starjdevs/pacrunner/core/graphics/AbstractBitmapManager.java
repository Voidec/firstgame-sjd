package com.starjdevs.pacrunner.core.graphics;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * Created by Void on 11.09.2014.
 */
abstract class AbstractBitmapManager implements BitmapManagerInterface {
    private Context myContext;
    private Bitmap bmp = null, bmp2;
    private AnimatedBitmap animBmp = null;
    private HashMap<String, Bitmap> BitMap;
    private HashMap<String, AnimatedBitmap> AnimatedBitMap;
    private Rect newR, oldR;
    public AbstractBitmapManager(Context myContext){
        this.myContext = myContext;
        BitMap = new HashMap<String, Bitmap>();
        AnimatedBitMap = new HashMap<String, AnimatedBitmap>();
    }
    public void resizeAnimation(String name, int newWidth, int newHeight){
        AnimatedBitMap.get(name).resize(newWidth, newHeight);


    }
    public void resizeImage(String name, int newWidth, int newHeight){
        newR = new Rect(0,0,newWidth, newHeight);
        bmp = BitMap.get(name);
        oldR = new Rect(0,0,bmp.getWidth(),bmp.getHeight());
        BitMap.remove(name);
        Bitmap convertedBitmap = Bitmap.createBitmap(newWidth, newHeight, bmp.getConfig());
        Canvas canvas = new Canvas(convertedBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawBitmap(bmp, oldR, newR, paint);
        BitMap.put(name, convertedBitmap);
    }


    public void createMap(String name, int x, int y, int width, int height, Bitmap.Config conf){
        bmp = Bitmap.createBitmap(x*width, y*height, conf);
        BitMap.put(name, bmp);
    }

    public void addTiles(String name, Tile[][] tiles, int x, int y){
        bmp = BitMap.get(name);
        Canvas canvas = new Canvas(bmp);
        Paint paint = new Paint();

        canvas.drawBitmap(bmp, 0, 0, paint);
        for (int i = 0; i < x; i++){
            for (int c = 0; c < y; c++){
                tiles[i][c].draw(canvas);}
        }
        BitMap.put(name, bmp);
    }
    @Override
    public void cutImage(String newName, String name, int startX, int startY, int newWidth, int newHeight) {
        newR = new Rect(0,0,newWidth, newHeight);
        bmp = BitMap.get(name);
        oldR = new Rect(startX,startY,bmp.getWidth(),bmp.getHeight());
        Bitmap convertedBitmap = Bitmap.createBitmap(bmp, startX, startY, newWidth, newHeight);
//        Canvas canvas = new Canvas(convertedBitmap);
//        Paint paint = new Paint();
//        paint.setColor(Color.BLACK);
//        canvas.drawBitmap(bmp, oldR, newR, paint);
        BitMap.put(newName, convertedBitmap);
    }

    public void load(String name, String path , Bitmap.Config config) {
        AssetManager asset = myContext.getAssets();
        try {
            InputStream is = asset.open("images/"+path +".png");

            bmp = BitmapFactory.decodeStream(is);

        } catch (IOException e) {
            e.printStackTrace();
        }
        setFormat(bmp, config);
        Bitmap formatBmp = setFormat(bmp, config);
        BitMap.put(name, formatBmp);
    }
    public void loadAnimation(String name, Bitmap bmp, Bitmap.Config config){
        animBmp = new AnimatedBitmap(bmp, config);
        AnimatedBitMap.put(name ,animBmp);
    }
    public void cropAnimation(String name, int startX, int startY, int width, int height, int frameCount){
        AnimatedBitMap.get(name).crop(startX, startY, width, height,frameCount);
    }
    public Bitmap setFormat(Bitmap bitmap, Bitmap.Config config) {
        Bitmap convertedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), config);
        Canvas canvas = new Canvas(convertedBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return convertedBitmap;
    }
    public void clear(String name) {
        BitMap.remove(name);
    }
    public void clearAll() {
        BitMap.clear();
    }
    public Bitmap get(String name){
        bmp = BitMap.get(name);
        return bmp;
    }
    public Bitmap getAnimation(String name){
        animBmp = AnimatedBitMap.get(name);
        bmp = animBmp.get();
        return bmp;
    }
    public HashMap<String, Bitmap> getMap(){
        return BitMap;
    }
    public void clearAnimation(String name) {
        AnimatedBitMap.remove(name);
    }
    public void clearAllAnimation() {
        AnimatedBitMap.clear();
    }
}